from typing import Optional
from fastapi import FastAPI, Request, APIRouter, HTTPException, Response, Body, Depends
import os, yaml, time, uvicorn
from models import ModelInfo, vehicleInformation, policyInformation, ValidationRoute, UnicornException
from helpers import featureTransformation
import pandas as pd
from fastapi.encoders import jsonable_encoder
from fastapi.routing import APIRoute
from fastapi.responses import JSONResponse, PlainTextResponse
from fastapi.exceptions import RequestValidationError, HTTPException
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.routing import Route

#start time to compute api response time 
start_time = time.time()
#Read yaml file 
with open("config.yml", "r") as configFile: 
    config = yaml.safe_load(configFile)

#use data sent to server to execute functions = post request


app = FastAPI()

router = APIRouter(route_class=ValidationRoute)
def get_coefficients():
    watchlist = pd.read_csv(config['default']['coefficients_file']) 
    return watchlist

@router.post("/getModelScore", dependencies = [Depends(get_coefficients)])
async def response(request: Request, policyInformation: policyInformation):
    request_response = {
        "method": request.method,
        "url": request.url,
        "headers": request.headers,
        "body": request.json()
    }
    features = featureTransformation(policyInformation)
    coefficients = get_coefficients()
    #j = await request.json()
    return features

app.include_router(router)



@app.get("/getVersions")
def versions():
    res = {
        "modelVersion": ModelInfo.PREDICTIVE_MODEL_VERSION,
        "modelVersionId": ModelInfo.PREDICTIVE_MODEL_VERSION_ID,
        "tag": ModelInfo.MODEL_TAG,
        "efd_threshold": ModelInfo.PREDICTIVE_MODEL_THRESHOLD     
     }   
    return res
if __name__ == '__efd__':
    uvicorn.run(app, host='127.0.0.1', port=8000, debug=True) 
    from typing import Optional

#curl --data @efd_data.json http://localhost:8000/getEFDScore