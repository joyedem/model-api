# to store all the models/classes (databases) that will be used in the API calls 
from enum import Enum
from pydantic import BaseModel
from typing import Optional, List
from fastapi.routing import APIRoute
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError, HTTPException
from fastapi import  Request, HTTPException, Response

class UnicornException(Exception):
    def __init__(self, name: str):
        self.name = name

class ValidationRoute(APIRoute):
    async def validations(request: Request) -> Response:
        try:
            if request.url.path not in ['/getEFDScore', '/getVersions']:
                return JSONResponse(
                status_code= 404,
                content = jsonable_encoder({"Error": "Invalid Endpoint"})
                )
        except RequestValidationError as exc:
            body = await request.body()
            detail = {"errors": exc.errors(), "body": body.decode()}
            raise HTTPException(status_code=422, detail= "Failed to parse request json")


class ModelInfo(str, Enum):
    MODEL_TAG = ""
    PREDICTIVE_MODEL_THRESHOLD = "0.011"
    PREDICTIVE_MODEL_VERSION = "1.0.2"
    PREDICTIVE_MODEL_VERSION_ID = "id1.0.2"
  
class addressList(BaseModel):
   addressId: str
   addressLine: str
   addressType: str
   city: str
   ciUnit: Optional[int] = None
   country: str
   state: str
   commercialIndicator: Optional[bool] = None
   latitude: str
   longitude: str
   nonSpecific: bool
   postalCode: str
   residentialFlag: str
   territorialFactorList: List[territorialFactorList]